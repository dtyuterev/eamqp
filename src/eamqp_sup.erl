
-module(eamqp_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init(_Args) ->
    {ok,{Name,SizeArgs,WorkerArgs }} = application:get_env(eamqp, pool),
    PoolSpec = poolboy:child_spec(Name, [{name, {local, Name}}, {worker_module, eamqp_srv}] ++ SizeArgs, WorkerArgs),
    lager:debug("~p",[PoolSpec]),
    {ok, {{one_for_one, 10, 10}, [PoolSpec]}}.

