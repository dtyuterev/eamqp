%%% Handles socket connections, and bridges a remote server
%%% With a progressquest game.
-module(eamqp_srv).
-include("amqp_client.hrl").
-behaviour(gen_server).
-behaviour(poolboy_worker).

-record(state, {
	ch = undefined,
	q = <<"">>,%undefined,
	%queue_declare = #'queue.declare'{},
    conn = undefined,
	conn_data = #amqp_params_network{}
}).
-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, 
         code_change/3, terminate/2]).

start_link(Args) ->
    gen_server:start_link(?MODULE, Args, []).

init(Args) ->
    %% properly seeding the process
    <<A:32, B:32, C:32>> = crypto:rand_bytes(12),
    random:seed({A,B,C}),
    Queue = proplists:get_value(queue,Args),
    Conn=#amqp_params_network{
 		username           = proplists:get_value(username,Args),
		password           = proplists:get_value(password,Args),
		virtual_host       = proplists:get_value(virtual_host,Args),
		host               = proplists:get_value(host,Args),
		port               = undefined,
		channel_max        = 0,
		frame_max          = 0,
		heartbeat          = 0,
		connection_timeout = 2000,
		ssl_options        = none,
		auth_mechanisms    =
		   [fun amqp_auth_mechanisms:plain/3,
		    fun amqp_auth_mechanisms:amqplain/3],
		client_properties  = [],
		socket_options     = [] 
    },
    %send to_self try_connect
    self()!try_connect,
    {ok, #state{conn_data=Conn,q=Queue}}.

handle_call({send,_Msg}, _From, #state{conn=undefined}=State) ->
      {reply, {error, "Connect not founded, sorry"}, State};
handle_call({send,Msg}, _From, #state{q=Q,ch=Channel}=State) ->
      %io:format("Here"),
      Publish = #'basic.publish'{exchange = <<"">>, routing_key = Q},
      amqp_channel:cast(Channel, Publish, #amqp_msg{payload = Msg}),
      {reply, ok, State};
handle_call({get}, _From, #state{q=Q,ch=Channel}=State) ->
      %io:format("Here"),
      Get = #'basic.get'{queue = Q},
      %{#'basic.get_ok'{delivery_tag = Tag}, Content} = amqp_channel:call(Channel, Get),
      Some = amqp_channel:call(Channel, Get),
      %{reply, {ok,{Tag,Content}}, State};
      {reply, {ok,{Some}}, State};
handle_call(_E, _From, State) ->
    {noreply, State}.

handle_cast({ack,Tag}, #state{ch=Channel}=State) ->
    amqp_channel:cast(Channel, #'basic.ack'{delivery_tag = Tag}),
    {noreply, State};
%handle_cast({subscribe,P}, State) ->
%    lager:debug("Got request to subscribe"),
%    self()!{subscribe,P},
%    {noreply, State};
handle_cast(_E, State) ->
    {noreply, State}.

handle_info(try_connect, #state{q=Q,conn_data=CD}=State) ->
    Timer = erlang:send_after(5000,self(),try_connect),
    case amqp_connection:start(CD) of
	{ok, Connection} 	->
		lager:info("Success connected to rabbitmq server"),
		erlang:cancel_timer(Timer),
		{ok, Channel} = amqp_connection:open_channel(Connection),
		QD=#'queue.declare'{queue=Q},
	      	#'queue.declare_ok'{queue=Q} = amqp_channel:call(Channel, QD),
    		{noreply, State#state{conn=Connection,ch=Channel}};
	{error, Reason}			->
		lager:error("Connection error : ~p",[Reason]),
		{noreply, State}
    end;
%------------------------------------------------------------------------------------
handle_info({subscribe,P}, #state{conn=undefined}=State) ->
    lager:warning("Get command to subscribe but have no connection, will try later"),
    erlang:send_after(10000,self(),{subscribe,P}),
    {noreply, State};
handle_info({subscribe,P}, #state{ch=undefined}=State) ->
    lager:warning("Get command to subscribe but have no channel, will try later"),
    erlang:send_after(10000,self(),{subscribe,P}),
    {noreply, State};
handle_info({subscribe,P}, #state{q=Q, ch=Channel}=State) ->
    lager:info("Get command to subscribe"),
	Sub = #'basic.consume'{queue = Q},
 	#'basic.consume_ok'{consumer_tag = _Tag} = amqp_channel:subscribe(Channel, Sub, P),
 	{noreply, State};
%------------------------------------------------------------------------------------
handle_info(_E, State) ->
    {noreply, State}.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

terminate(normal, _State) ->
    ok;
terminate(_Reason, _State) ->
    io:format("terminate reason: ~p~n", [_Reason]).

