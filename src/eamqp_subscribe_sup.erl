
-module(eamqp_subscribe_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).
-export([start_sub/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init(_Args) ->
    {ok, {{simple_one_for_one, 10, 10}, [?CHILD(eamqp_subscribe_srv,worker)]}}.

start_sub(Pid) ->
    lager:info("Get command to start new subscribe worker from ~p",[Pid]),
    supervisor:start_child(?MODULE,[{pid,Pid}]).
    

