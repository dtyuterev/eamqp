-module(eamqp).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).
-export([send_msg/1,get_msg/0,subscribe/1,ack_msg/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    eamqp_subscribe_sup:start_link(),
    eamqp_sup:start_link().

stop(_State) ->
    ok.

-spec send_msg(binary()) -> ok.
send_msg(Msg) when is_binary(Msg)->
    	{ok,{Name,_,_ }} = application:get_env(eamqp, pool),
    	poolboy:transaction(Name, fun(Worker) -> gen_server:call(Worker, {send, Msg}) end);
send_msg(Msg) ->
    lager:error("It's not binary: ~p",[Msg]),
    {error,"Sended Message should be binary() type"}.

get_msg()->
    {ok,{Name,_,_ }} = application:get_env(eamqp, pool),
	poolboy:transaction(Name, fun(Worker) -> gen_server:call(Worker, {get}) end).

-spec subscribe(PID :: pid()) -> ok | {error, Reason :: term()}.
subscribe(Pid) when is_pid(Pid) ->
	case eamqp_subscribe_sup:start_sub(Pid) of
		{ok, NewPid} ->
                lager:info("Successfull create new subscriber_srv"),
    			{ok,{Name,_,_ }} = application:get_env(eamqp, pool),
                lager:debug("PoolName : ~p",[Name]),
			    %poolboy:transaction(Name, fun(Worker) -> gen_server:cast(Worker, {subscribe,NewPid}) end);
			    poolboy:transaction(Name, fun(Worker) -> Worker!{subscribe,NewPid} end),
                ok;
		Msg ->
                lager:error("Error while create subscriber : ~p",[Msg]), 
                {error,Msg}
	end;
subscribe(Pid)->
    lager:error("It's not pid : ~p",[Pid]),
    {error, "Argument should be pid() type"}.

ack_msg(Tag)->
    	{ok,{Name,_,_ }} = application:get_env(eamqp, pool),
	    poolboy:transaction(Name, fun(Worker) -> gen_server:cast(Worker, {ack,Tag}) end).

