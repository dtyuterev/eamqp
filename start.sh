#!/bin/sh

erl \
  -sname eamqp_test_node \
  -config priv/app.config \
  -pa ../*/ebin ./deps/*/ebin \
  -eval 'lists:foreach(fun(App) -> ok = application:start(App) end, [ syntax_tools, compiler, goldrush, lager, eamqp ])' 
