ERL=erl
REBAR=rebar

.PHONY: deps get-deps test

compile:
	ln -s /home/dtyuterev/rabbit_common deps/rabbit_common
	ln -s /home/dtyuterev/amqp_client deps/amqp_client
	@$(REBAR) compile skip_deps=true

clean:
	@$(REBAR) clean

get-deps:
	@$(REBAR) get-deps
	ln -s /home/dtyuterev/rabbit_common deps/rabbit_common
	ln -s /home/dtyuterev/amqp_client deps/amqp_client

deps:
	ln -s /home/dtyuterev/rabbit_common deps/rabbit_common
	ln -s /home/dtyuterev/amqp_client deps/amqp_client
	@$(REBAR) compile

test:
	@$(REBAR) skip_deps=true eunit
